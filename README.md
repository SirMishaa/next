# # Next
Next est un bot qui permet de générer des statistiques sur votre serveur Discord. Il est configurable via une interface Web. 

## Build with
*Dernière modification : 19 septembre 2019*
**Next est développé avec les technologies suivantes :** 

* [**Discord JS**]([https://discord.js.org/#/](https://discord.js.org/#/)) - Une puissante librairie pour interagir avec l'api de Discord.
* [**TypeScript**]([https://www.typescriptlang.org/](https://www.typescriptlang.org/)) - Un preset de Javascript qui ajoute un système de typage primitif ainsi d'autres fonctionnalités.   
* [**Express**]([https://expressjs.com/fr/](https://expressjs.com/fr/)) - Une infrastructure d'application Web qui fournit un ensemble de fonctionnalités robuste pour la création de site internet.
* [**React**]([https://fr.reactjs.org/](https://fr.reactjs.org/)) - Une librairie pour créer des interfaces  utilisateurs puissantes 
* ... Certains éléments doivent encore être décidé, comme l'ORM par exemple.