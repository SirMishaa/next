import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class ObservedMessage {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    messageID: string;

    @Column()
    channelID: string;

    @Column()
    guildID: string;

    @Column()
    emoteName: string;
}