import * as moment from "moment";
import 'moment/locale/fr';

const chalk = require('chalk');

export class OutputManager {

    public static log(message: string, type ?: number): void {
        if ( !type ) {
            const type: number = 0
        }
        moment.locale('fr');
        const now = moment().format('HH:mm:ss');
        switch (type) {
            case 1:
                console.log(`[${now}][INFO] ${message}`);
                break;
            case 2:
                console.log(`[${now}]${chalk.yellow('[WARN] ' + message)}`);
                break;
            case 3:
                console.log(`[${now}]${chalk.red('[ERR] ' + message)}`);
                break;
            default:
                console.log(`[${now}] ${message}`);
                break
        }
    }
}