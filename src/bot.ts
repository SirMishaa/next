import {Channel, Client, Collection, GuildMember, Message, RichEmbed, TextChannel} from 'discord.js';
import * as path from 'path';
import * as YAML from 'yamljs';
import {OutputManager} from "./iLog/OutputManager";
import {CommandHandler} from "./Commands/CommandHandler";
import {Database} from "./Class/Database";
import {ObservedMessage} from "./entity/ObservedMessage";


interface ObsevedMessageInterface {
    id: number,
    messageID: string,
    channelID: string,
    guildID: string,
    emoteName: string,
}

export class DiscordServer {
    private readonly client: Client;
    private readonly clientCommand: Collection<any, any>;
    private config: any;
    private readonly isOnMaintenance: boolean;
    private readonly prefix: string;
    private static DiscordServerInstance: DiscordServer;
    public observedMessage: Array<object>;


    private constructor() {
        this.client = new Client();
        this.clientCommand = new Collection();
        this.config = YAML.load(path.resolve(__dirname, 'settings.yml'));
        this.isOnMaintenance = this.config.settings.isOnMaintenance;
        this.prefix = this.config.settings.prefix;
    }

    public static getDiscordServerInstance(): DiscordServer {
        if ( !this.DiscordServerInstance )
            this.DiscordServerInstance = new DiscordServer();
        return this.DiscordServerInstance;
    }


    public start(): void {
        this.client.on('ready', () => {

            Database.init().then(async conn => {
                const ObsevedMessage = conn.getRepository(ObservedMessage);
                this.observedMessage = await ObsevedMessage.find();
                // @ts-ignore
                this.observedMessage.forEach((message: ObsevedMessageInterface) => {
                    const channel: Channel | TextChannel = this.client.channels.get(message.channelID);
                    // @ts-ignore
                    channel.fetchMessage(message.messageID)
                        .then((message: Message) => OutputManager.log('Fetching message with id ' + message.id))
                        .catch((error: Error) => OutputManager.log('Unable to fetch message. ' + error))
                    // Todo : Remove automatically old monitored message from database when they don't exist anymore
                    // Todo : Make command for getting a list of monitored message
                    // Todo : Fetch message at init for adding role on user have added a reaction on message
                });
                await conn.close();
            });
            this.client.user.setActivity(this.config.settings.activity).catch((err) => console.log(err));
            const commandHandler = CommandHandler.getInstance();
            try {
                commandHandler.loadCommands(this.client, this.clientCommand);
                const UsersOnline: number = this.client.users.filter(user => String(user.presence.status) !== 'offline' && !user.bot).size;
                OutputManager.log(`There is ${UsersOnline} users connected to the discord server.`, 1)
            } catch (e) {
                throw new Error(e);
            }
        });

        this.client.on('message', async (message: Message) => {
            if ( message.author.bot ) return;
            if ( message.type === 'PINS_ADD' && message.author.bot ) return message.delete();

            const messageContent = message.content;
            const args = messageContent.split(' ');
            const commandName = args[0].slice(this.prefix.length);
            args.shift();

            if ( !messageContent.startsWith(this.prefix) ) return;
            if ( this.isOnMaintenance ) {
                const embed: RichEmbed = new RichEmbed()
                    .setColor(15158332)
                    .setTitle("Bot en maintenance.")
                    .setDescription("Nous sommes désolé mais le bot est mit en mode maintenance et seul le développeur peut exécuté des commandes.")
                    .setFooter(`Exception levée par ${message.member.displayName}`);
                OutputManager.log('Commaned cancelled because the bot is on maintenance mod', 3);
                return message.channel.send({embed});
            }
            if ( !this.clientCommand.get(commandName) ) {
                const embed: RichEmbed = new RichEmbed()
                    .setColor(15158332)
                    .setTitle("Commande inconnue.")
                    .setDescription("Cette commande n'existe pas, vous pouvez faire &help afin d'obtenir la liste des commandes disponible.")
                    .setFooter(`Exception levée par ${message.member.displayName}`);
                return message.channel.send({embed});
            }
            this.clientCommand.get(commandName).execute(this.client, message, args);

        });

        this.client.on('messageReactionAdd', (event, user) => {
            const message = event.message;
            const messageMember: GuildMember = message.guild.members.get(user.id);
            const messageID: string = event.message.id;
            const emote: string = `${event.emoji}`;

            if ( this.isOnMaintenance ) {
                OutputManager.log('Event cancelled because the bot is on maintenance mod', 3);
                return;
            }

            this.observedMessage.forEach((message: ObsevedMessageInterface) => {
                if ( message.messageID === messageID && message.emoteName === emote ) {
                    if ( !messageMember.roles.has(message.guildID) ) {
                        messageMember.addRole(message.guildID)
                            .then((messageMember) => OutputManager.log(`User ${messageMember.displayName} get ${message.guildID} role !`, 1))
                            .catch(() => OutputManager.log(`Unable to give role ${message.guildID} to ${messageMember.displayName}`))
                    }
                }
            })

        });


        this.client.login(this.config.settings.token).then(() => {
            OutputManager.log("Successful connection, the bot is now online.", 1);
            this.isOnMaintenance ? OutputManager.log('Bot is on maintenance mod', 3) : ''
        }).catch(err => console.log(err));
    }

}