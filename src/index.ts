import "reflect-metadata";
import {DiscordServer} from './bot';
import * as express from 'express';
import {Request, Response} from 'express';
import {OutputManager} from "./iLog/OutputManager";

const app: express.Application = express();
const bot: DiscordServer = DiscordServer.getDiscordServerInstance();

app.get('/', (req: Request, res: Response): void => {
    res.send();
});

app.listen(3000, () => {
    OutputManager.log('Web server listening on port 3000', 1);
});
bot.start();