import {Connection, createConnection} from "typeorm";
import {ObservedMessage} from "../entity/ObservedMessage";

export class Database {
    public static instance: Database;

    private constructor() {
    }

    public static init(): PromiseLike<Connection> {
        return createConnection({
            type: "mariadb",
            host: "localhost",
            port: 3306,
            username: "root",
            password: "root",
            database: "next_dev",
            entities: [
                ObservedMessage
            ],
            synchronize: true,
            logging: false
        })
    }

    public static getDatabaseInstance() {
        if ( !this.instance )
            this.instance = new Database();
        return this.instance
    }

}