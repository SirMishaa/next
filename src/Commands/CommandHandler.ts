import * as fs from "fs";
import {Client, Collection} from "discord.js";
import {OutputManager} from "../iLog/OutputManager";

export class CommandHandler {
    private static instance: CommandHandler;

    private constructor() {

    }

    public static getInstance() {
        !CommandHandler.instance ? CommandHandler.instance = new CommandHandler() : CommandHandler.instance;
        return CommandHandler.instance;
    }

    public loadCommands(client: Client, clientCommand: Collection<any, any>) {
        let commandFiles = fs.readdirSync('./Commands').filter((file: any) => file.endsWith('.js'));
        commandFiles = commandFiles.filter(i => i !== 'CommandHandler.js');
        OutputManager.log(`${commandFiles.length} commands files have to be loaded.`, 1);
        console.log(commandFiles);
        commandFiles.forEach((file) => {
            const command = require(`./${file}`);
            clientCommand.set(command.name, command);
        });
        OutputManager.log(`Successful loading.`, 1);
    }
}