import {Client, Message, RichEmbed} from "discord.js";
import * as moment from "moment";


module.exports = {
    name: 'ping',
    description: "Permet d'obtenir le temps de latence du bot et de l'api discord.",
    execute: async function (client: Client, message: Message) {
        const initialTime: number = moment().toDate().getTime();
        message.channel.send("Pong").then(msg => {
            const apiPing: number = Math.round(moment().toDate().getTime() - initialTime);
            let color: number = 0;
            if ( apiPing < 200 ) {
                color = 3066993
            } else if ( apiPing > 200 && apiPing < 450 ) {
                color = 15844367;
            } else {
                color = 15158332;
            }
            const embed: RichEmbed = new RichEmbed()
                .setColor(color)
                .addField('API ping: ', apiPing + 'ms')
                .addField('Bot ping: ', client.ping + 'ms')
                .setFooter(`Commande exécutée par ${message.member.displayName}`);
            message.channel.send({embed});
        });
    }
};

