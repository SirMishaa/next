import {Client, Message, RichEmbed} from "discord.js";

module.exports = {
    name: 'mute',
    description: '',
    execute: async function (client: Client, message: Message, args?: Array<string>) {

        let timeValue = "1";
        if ( message.member.roles.has('627483655698382878') || message.member.roles.has('547899307949424650') ) {

            if ( args && args.length < 0 || args.length > 2 ) {
                const embed: RichEmbed = new RichEmbed()
                    .setColor(15158332)
                    .setTitle("Format de commande invalide.")
                    .setDescription("Vous devez former la commande de la façon suivante : &mute <@user> <10>. Le temps doit-être défini en heure.")
                    .setFooter(`Exception levée par ${message.member.displayName}`);
                return message.channel.send({embed});
            }

            if ( args && args[1] ) {
                timeValue = args[1];
            }

            const memberToBeMuted = message.mentions.members.first();
            const mutedRole = message.guild.roles.get('549348370452578306');

            if ( !memberToBeMuted.roles.has(mutedRole.id) ) {
                memberToBeMuted.addRole(mutedRole.id).then(() => {
                    message.channel.send(
                        `${memberToBeMuted} vient d'être rendu au silence pour une durée de **${
                            timeValue
                        } heure(s).**`
                    );
                }).catch(err => {
                    const embed: RichEmbed = new RichEmbed()
                        .setColor(15158332)
                        .setTitle("Une erreur inconnue s'est produite.")
                        .setDescription(err)
                        .setFooter(`Exception levée par ${message.member.displayName}`);
                    return message.channel.send({embed});
                })

            }

            const value = parseInt(timeValue);

            setTimeout(() => {
                if ( memberToBeMuted.roles.has(mutedRole.id) ) {
                    memberToBeMuted.removeRole(mutedRole.id)
                }
            }, value * 60 * 60 * 1000
            )


        } else {
            const embed: RichEmbed = new RichEmbed()
                .setColor(15158332)
                .setTitle("Vous n'avez pas les permissions pour effectuer cette commande.")
                .setDescription("Vous devez être administrateur pour effectuer cette commande.")
                .setFooter(`Exception levée par ${message.member.displayName}`);
            return message.channel.send({embed})
        }
    }
};