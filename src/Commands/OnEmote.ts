import {Channel, Client, Message, RichEmbed, TextChannel} from "discord.js";
import {Database} from "../Class/Database";
import {ObservedMessage} from "../entity/ObservedMessage";
import {OutputManager} from "../iLog/OutputManager";
import {DiscordServer} from "../bot";


module.exports = {
    name: 'onemote',
    description: '',
    execute: function (client: Client, message: Message, args?: Array<string>) {
        if ( args && args.length === 1 && args[0].toLowerCase() === 'list' ) {
            Database.init().then(async c => {
                const arrayOfMessage = await c.getRepository(ObservedMessage).find();
                const listEmbed: RichEmbed = new RichEmbed();
                let x: number = 0;
                arrayOfMessage.forEach((message: ObservedMessage) => {
                    x++;
                    // Todo : Refactor and get guild from id if still exist
                    listEmbed.addField(`Pour l'ajout du rôle ${message.guildID}`,
                        `Sur le message ayant comme ID **${message.messageID}** lors de la réaction ${message.emoteName}`)
                });
                if ( x > 0 ) {
                    listEmbed.setTitle(`Listing de ${x} messages en surveillance`);
                    listEmbed.setColor('#0099ff');
                } else {
                    listEmbed.setTitle("Il n'y a pas de message en surveillance pour l'instant.");
                    listEmbed.setColor(15158332);
                    listEmbed.addField('Ajouter un message à surveiller', 'Vous pouvez utiliser la commande &onemote pour surveiller un message.')
                }
                await c.close();
                message.channel.send(listEmbed);
            });
            return true;
        }

        if ( args && args.length !== 3 && args[0].toLowerCase() !== 'list' ) {
            const embed: RichEmbed = new RichEmbed()
                .setColor(15158332)
                .setTitle("Format de commande invalide.")
                .setDescription("Vous devez former la commande de la façon suivante : &onemote <messageid> <:emotename:> <guildid>")
                .setFooter(`Exception levée par ${message.member.displayName}`);
            return message.channel.send({embed});
        }

        // Command shema : &onemote <messageid> <:emotename:> <guildid>
        if ( message.member.roles.has('547899307949424650') || message.member.roles.has('627483655698382878') ) {
            if ( !args[1].match('(\\:.*?\\:)') ) {
                const embed: RichEmbed = new RichEmbed()
                    .setColor(15158332)
                    .setTitle("Erreur de syntaxe")
                    .setDescription('Le nom de l\'emote doit être entouré par \':\'')
                    .setFooter(`Exception levée par ${message.member.displayName}`);
                return message.channel.send({embed});
            }
            message.channel.fetchMessage(args[0]).then(message => {
                const role = message.guild.roles.get(args[2]);
                if ( role ) {
                    // Todo : Refactor
                    Database.init().then(connection => {
                        let observecMessage = new ObservedMessage();
                        observecMessage.messageID = args[0];
                        observecMessage.emoteName = args[1];
                        observecMessage.guildID = args[2];
                        observecMessage.channelID = message.channel.id;
                        return connection.manager
                            .save(observecMessage)
                            .then(async observedMessage => {
                                OutputManager.log(`Observed message <${observedMessage.id}> has been saved into the database.`, 1);
                                DiscordServer.getDiscordServerInstance().observedMessage.push(observedMessage);
                                const channel: Channel | TextChannel = client.channels.get(observedMessage.channelID);
                                // @ts-ignore
                                channel.fetchMessage(observedMessage.messageID);
                                await connection.close()
                            });
                    });

                    const embed: RichEmbed = new RichEmbed()
                        .setColor(3066993)
                        .setTitle("Succès.")
                        .setDescription(`Le message avec l'identifiant " **${args[0]}** " est maintenant surveillé, si l'emote correspond bien à " ${args[1]} ", le role ayant comme nom " **${role.name}** " lui sera automatiquement ajouté.`)
                        .setFooter(`Commande exécutée par ${message.member.displayName}`);
                    message.delete();
                    return message.channel.send({embed});
                } else {
                    const embed: RichEmbed = new RichEmbed()
                        .setColor(15158332)
                        .setTitle("Le role n'existe pas.")
                        .setDescription('Veuillez d\'abord créer le rôle.')
                        .setFooter(`Exception levée par ${message.member.displayName}`);
                    return message.channel.send({embed});
                }


                // DiscordServer.modelCreator(args[0], args[1],args[2]);

            }).catch(err => {
                const embed: RichEmbed = new RichEmbed()
                    .setColor(15158332)
                    .setTitle("Une erreur inconnue s'est produite.")
                    .setDescription(err)
                    .setFooter(`Exception levée par ${message.member.displayName}`);
                return message.channel.send({embed});
            });

        } else {
            const embed: RichEmbed = new RichEmbed()
                .setColor(15158332)
                .setTitle("Vous n'avez pas les permissions pour effectuer cette commande.")
                .setDescription("Vous devez être administrateur pour effectuer cette commande.")
                .setFooter(`Exception levée par ${message.member.displayName}`);
            return message.channel.send({embed});
        }
    }
};