import {Message} from "discord.js";

module.exports = {
    name: 'help',
    description: 'Provides some useful information',
    async execute(message: Message, args: Array<string>) {
        const embed = {
            "title": "Aide",
            "description": "a",
            "color": 4740730,
            "footer": {
                "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png",
                "text": "Commande exécutée par " + message.author
            },
            "thumbnail": {
                "url": "https://cdn.discordapp.com/embed/avatars/0.png"
            }
        };
        message.channel.send({embed});
    },
};